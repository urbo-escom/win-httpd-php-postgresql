Installar postgres en Msys2
---------------------------

Antes de  instalar cualquier cosa  siempre es bueno  verificar actualizaciones
con:

	$ pacman -Syu

si se te pide instalar algo, debes seguir las instrucciones.

Para la instalación de postgresl, se puede buscar el paquete con:

	$ pacman -Ss postgres

y de ahí escoger el adecuado según  la arquitectura (32bit vs 64bit), luego se
instala con `pacman -S <nombre del paquete>`:

	# 32 bit
	$ pacman -S mingw-w64-i686-postgresql
	# 64 bit
	$ pacman -S mingw-w64-x86_64-postgresql



Configurar postgres
-------------------

Una vez instalado se debe inicializar  un cluster, para esto necesitamos saber
algunas  cosas como  que  tipo  de autentificación  queremos,  el lugar  donde
guardaremos  los archivos,  la  codificación de  la base,  la  colación, y  el
usuario:

	$ initdb \
	>         --auth password \
	>         --pgdata=tmp/data \
	>         --encoding=UTF-8 \
	>         --locale=spanish-mexican \
	>         -U postgres -W

El  comando anterior  creará un  cluster con  autentificación con  contraseña,
guardará  los archivos  en  el  directorio tmp/data  relativo  al lugar  donde
ejecutemos el comando, con UTF-8 como  codificación y el español mexicano como
colación,  aparte  creará  el  usuario  postgres. El  comando  te  pedirá  una
contraseña, esa es la misma que se usará para conectarse a través de php.

- El directorio del cluster va a  ser creado pero puede que no sus directorios
padre.

- Si la  colación no  está  presente en   la  computadora, se  puede omitir  y
postgres  usará la  del sistema,  el nombre  `spanish-mexican` existe  solo en
Windows, para sistemas tipo-unix se emplea `es_MX` ó `es_MX.UTF-8`.



Iniciar y detener el servidor postgres
--------------------------------------

Para  iniciar el  servidor  se necesita  saber dónde  está  el directorio  del
cluster y dónde se quieren almacenar los logs:

	$ pg_ctl start -D tmp/data -l tmp/pg.log
	server starting

El  comando debe  terminar casi  inmediatamente, el  servidor se  separará del
proceso que lo creó.

Para los siguientes  comandos es necesario recordar el  directorio del cluster
usado para iniciar el servidor.

Para saber si está  corriendo se usa la misma aplicación  pero con `status` en
vez de `start`:

	$ pg_ctl status -D tmp/data
	pg_ctl: server is running (PID: dddd)
	...postgres.exe "-D" "tmp/data"

Para apagar el servidor se debe usar:

	$ pg_ctl stop -D tmp/data
	waiting for server to shut down.... done
	server stopped
	$ pg_ctl status -D tmp/data
	pg_ctl: no server running



Acceder a la consola de postgres
--------------------------------

Con el servidor ya iniciado se debe correr:

	$ psql -U postgres
	...
	postgres=# \q

se debe salir de la consola con '\q', si se desea omitir la contraseña se debe
colocar en una variable de ambiente:

	$ export PGPASSWORD=qwerty
	$ psql -U postgres
	...
	$ PGPASSWORD=qwerty psql -U postgres
	...
	$ PGPASSWORD="contraseña con espacios" psql -U postgres
	...

La consola probablemente  mostrará errores relacionados con el  `code page` de
Windows, este error es debido a que  la consola no soporta utf8, no hay manera
de arreglar  este error a  menos que  se corra psql  dentro de cmd.exe  con el
`code  page` correcto,  pero  la  interfaz de  psql  compilada  para msys2  no
funciona con cmd.exe y no se verá nada (aunque sigue funcionando la consola).

	C:\Users\qwerty> chcp 65001
	...
	C:\Users\qwerty> psql ...
	qwerty
	select 'É';
	?column?
	--------
	    É
	...

El error de  `code page` se puede  ignorar hasta que se  usen caracteres fuera
del límite ascii:

	$ psql -U postgres ...
	...
	postgres=# select 'É';
	ERROR:  character with byte sequence 0x90 in encoding "WIN1252" has
	no equivalent in encoding "UTF8"
	postgres=# show client_encoding;
	 client_encoding
	-----------------
	 WIN1252
	(1 row)
	postgres=# set client_encoding to 'UTF-8';
	SET

[TODO]...



Crear una base de datos
-----------------------

Conectarse con psql:

	$ psql -U postgres
	Password for user postgres: ******
	psql (9.5.2)
	WARNING: Console code page (850) differs from Windows code page (1252)
		 8-bit characters might not work correctly. See psql reference
		 page "Notes for Windows users" for details.
	Type "help" for help.

	postgres=# -- comentarios empiezan con --
	postgres=# create database foo
	postgres=#         encoding = 'UTF8'
	postgres=#         lc_collate = 'Spanish_Mexico.1252'
	postgres=#         lc_ctype = 'Spanish_Mexico.1252'
	postgres=#         template=template0;
	CREATE DATABASE
	postgres=# \c foo

	You are now connected to database "foo" as user "postgres".
	foo=# -- se pueden crear tablas
	foo=# -- ...
	foo=# -- conectarse a otra base
	foo=# \c postgres

	You are now connected to database "postgres" as user "postgres".
	postgres=# drop database foo;
	DROP DATABASE
