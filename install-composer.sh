#!/bin/sh

self="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)"

# echo \
# cd "$self" >&2
# cd "$self"

mkdir_or_die() {
	while [ "$#" != "0" ]; do
		if [ ! -e "$1" ]; then
			echo \
			mkdir "$1" >&2
			mkdir "$1" || exit 1
		elif [ ! -d "$1" ]; then
			echo "$1: is not a directory" >&2
			exit 1
		fi
		shift
	done
}

mkdir_or_die \
	dist/ \
	dist/composer/ \
	tmp/ \


SIG=$(wget https://composer.github.io/installer.sig -O - -q)
php -r "copy('https://getcomposer.org/installer', 'tmp/composer-setup.php');"
FILE_SIG=$(php -r "echo hash_file('SHA384', 'tmp/composer-setup.php');")

if [ "$SIG" != "$FILE_SIG" ]; then
	echo "File signature did not match signature: $FILE_SIG != $SIG" >&2
	rm tmp/composer-setup.php
	exit 1
else
	php tmp/composer-setup.php -- \
		--install-dir=dist/composer \
		--filename=composer
	status=$?
	rm tmp/composer-setup.php
	exit $status
fi
