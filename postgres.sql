drop database if exists foo;
create database foo
	encoding = 'UTF8'
	lc_collate = 'Spanish_Mexico.1252'
	lc_ctype   = 'Spanish_Mexico.1252'
	template = template0;
