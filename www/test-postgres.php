<?php
$pgdatabase = getenv('PGDATABASE');
$pgpassword = getenv('PGPASSWORD');
$pguser     = getenv('PGUSER');


header("Content-Type: text/html; charset=utf-8");
$con_str =
	"dbname=$pgdatabase ".
	"user=$pguser ".
	"password=$pgpassword ";
$con = pg_connect($con_str);

function show_query($con, $q) {
	$res = pg_query($q);
	if (false === $res) {
		echo "<pre>pg_query: &lt;$q&gt;",
			pg_last_error($con),
			"\n</pre>\n";
		return;
	}
	echo "<pre>$q =&gt; ",
		json_encode(pg_fetch_all($res), JSON_PRETTY_PRINT),
		"\n</pre>";
}

show_query($con, "show server_encoding");
show_query($con, "show client_encoding");
show_query($con, "create temporary table b (i serial primary key, n integer)");
show_query($con, "insert into b (n) values (54), (200) returning i, n");
show_query($con, "insert into b (n) values (2) returning i, n");
show_query($con, "insert into b (n) values (2) returning i, n");
show_query($con, "insert into b (n) values (2) returning i, n");
show_query($con, "insert into b (n) values (2) returning i, n");
show_query($con, "insert into b (n) values (2) returning i, n");
