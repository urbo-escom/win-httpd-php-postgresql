#
# Ver hasta abajo las partes que se pueden configurar (con override)
#
# Para correr el servidor:
#
#         $ make run
#
# para terminarlo basta Ctrl-C
#
# Pasar opciones:
#
#         $ make run IPADDR=192.168.56.1     IPPORT=80
#         $ make run IPADDR_SSL=192.168.56.1 IPPORT_SSL=443
#         $ make run WEBROOT=/c/Users/name/Desktop/web-root
#         $ make run WEBROOT_SSL=/c/Users/name/Desktop/web-root
#         $ make run LOG_DIR=/c/Users/name/Desktop/web-logs
#
# Nota: el servidor inicia tanto la versión SSL como la versión sin SSL
#
root := $(patsubst %/,%,$(dir $(lastword ${MAKEFILE_LIST})))


# Crea cualquier directorio que necesitemos y no exista
%/:
	$(if $(wildcard $@.),@echo $@ exists,mkdir $@)


# Descarga de apache y php
.PHONY: download
UA := Mozilla/5.0 (Windows NT 6.3; rv:48.0) Gecko/20100101 Firefox/48.0


HTTPD_ZIP := ${root}/tmp/httpd-2.4.23-win64-VC14.zip
HTTPD_DIR := ${root}/dist/httpd-2.4.23
f := httpd-2.4.23-win64-VC14.zip
download: tmp/$f
tmp/$f: URL := https://www.apachelounge.com/download/VC14/binaries/$f
tmp/$f: | tmp/
	$(if $(wildcard $@),@echo $@ exists, \
		wget --user-agent="${UA}" -O"$@" "${URL}")


PHP_ZIP := ${root}/tmp/php-7.0.11-Win32-VC14-x64.zip
PHP_DIR := ${root}/dist/php-7.0.11
f := php-7.0.11-Win32-VC14-x64.zip
download: tmp/$f
tmp/$f: URL := http://windows.php.net/downloads/releases/$f
tmp/$f: | tmp/
	$(if $(wildcard $@),@echo $@ exists, \
		wget --user-agent="${UA}" -O"$@" "${URL}")



# Descompresión de zips
.PHONY: unzip-all
unzip-all: ${HTTPD_DIR}/Apache24/bin/httpd.exe
unzip-all: ${PHP_DIR}/php.exe

${HTTPD_DIR}/: | ${root}/dist/
${HTTPD_DIR}/Apache24/bin/httpd.exe: DIR := ${HTTPD_DIR}/
${HTTPD_DIR}/Apache24/bin/httpd.exe: ZIP := ${HTTPD_ZIP}
${HTTPD_DIR}/Apache24/bin/httpd.exe: | ${HTTPD_ZIP}
${HTTPD_DIR}/Apache24/bin/httpd.exe: | ${HTTPD_DIR}/
	unzip -x -d ${DIR} -o ${ZIP}

${PHP_DIR}/: | ${root}/dist/
${PHP_DIR}/php.exe: DIR := ${PHP_DIR}/
${PHP_DIR}/php.exe: ZIP := ${PHP_ZIP}
${PHP_DIR}/php.exe: | ${PHP_ZIP}
${PHP_DIR}/php.exe: | ${PHP_DIR}/
	unzip -x -d ${DIR} -o ${ZIP}



# Generación de llave privada y certificado autofirmado
# Ninguna de esta información es importante, solo SSL_CN
override SSL_C  ?= MX
override SSL_ST ?= CDMX
override SSL_L  ?= CDMX
override SSL_O  ?= me
override SSL_OU ?= me
override SSL_CN ?= localhost
SSL_SUBJ :=
SSL_SUBJ := ${SSL_SUBJ}//C=${SSL_C}
SSL_SUBJ := ${SSL_SUBJ}\ST=${SSL_ST}
SSL_SUBJ := ${SSL_SUBJ}\L=${SSL_L}
SSL_SUBJ := ${SSL_SUBJ}\O=${SSL_O}
SSL_SUBJ := ${SSL_SUBJ}\OU=${SSL_OU}
SSL_SUBJ := ${SSL_SUBJ}\CN=${SSL_CN}

.PHONY: ssl-gen
ssl-gen: ${root}/ssl/key.pem
ssl-gen: ${root}/ssl/csr.pem
ssl-gen: ${root}/ssl/cert.pem
ssl-gen:
	openssl rsa  -in $(word 1, $^) -check
	openssl req  -in $(word 2, $^) -text -noout -verify
	openssl x509 -in $(word 3, $^) -text -noout

${root}/ssl/key.pem: | ${root}/ssl/
	openssl genrsa -out $@ 1024

${root}/ssl/csr.pem: ${root}/ssl/key.pem
	openssl req -new -key $< -out $@ \
		-subj "${SSL_SUBJ}"

${root}/ssl/cert.pem: ${root}/ssl/key.pem ${root}/ssl/csr.pem
	openssl x509 -req \
		-signkey $(word 1, $^) \
		-in      $(word 2, $^) \
		-out $@



# Para correr
.PHONY: run
run: unzip-all ssl-gen

# configurable
cygpath2win = $(patsubst %/,%,$(subst \,/,$(shell cygpath -wa $1)))
override WEBROOT     ?= ${root}/www
override WEBROOT_SSL ?= ${root}/www
override HOST        ?= localhost
override IPADDR      ?= 127.0.0.1
override IPPORT      ?= 80
override IPADDR_SSL  ?= 127.0.0.1
override IPPORT_SSL  ?= 443
override LOG_DIR     ?= ${root}/logs

run: HTTPD_WEBROOT     := $(call cygpath2win, ${WEBROOT})
run: HTTPD_WEBROOT_SSL := $(call cygpath2win, ${WEBROOT_SSL})
run: HTTPD_SERVERNAME  := ${HOST}
run: HTTPD_IPADDR      := ${IPADDR}
run: HTTPD_IPPORT      := ${IPPORT}
run: HTTPD_IPADDR_SSL  := ${IPADDR_SSL}
run: HTTPD_IPPORT_SSL  := ${IPPORT_SSL}
run: HTTPD_LOG_DIR     := $(call cygpath2win, ${LOG_DIR})
run: PHP_LOG_DIR       := $(call cygpath2win, ${LOG_DIR})

run: HTTPD_PHP7_MOD := $(call cygpath2win, ${PHP_DIR}/php7apache2_4.dll)
run: HTTPD_SSL_CERT := $(call cygpath2win, ${root}/ssl/cert.pem)
run: HTTPD_SSL_KEY  := $(call cygpath2win, ${root}/ssl/key.pem)
run: PHP_EXTDIR     := $(call cygpath2win, ${PHP_DIR}/ext)
run: ${root}/conf/httpd.conf ${root}/conf/php.ini
	cat $(word 1, $^) | \
		sed 's#HTTPD_WEBROOT_SSL#${HTTPD_WEBROOT_SSL}#g' | \
		sed 's#HTTPD_WEBROOT#${HTTPD_WEBROOT}#g' | \
		sed 's#HTTPD_SERVERNAME#${HTTPD_SERVERNAME}#g' | \
		sed 's#HTTPD_IPADDR_SSL#${HTTPD_IPADDR_SSL}#g' | \
		sed 's#HTTPD_IPPORT_SSL#${HTTPD_IPPORT_SSL}#g' | \
		sed 's#HTTPD_IPADDR#${HTTPD_IPADDR}#g' | \
		sed 's#HTTPD_IPPORT#${HTTPD_IPPORT}#g' | \
		sed 's#HTTPD_LOG_DIR#${HTTPD_LOG_DIR}#g' | \
		sed 's#HTTPD_PHP7_MOD#${HTTPD_PHP7_MOD}#g' | \
		sed 's#HTTPD_SSL_CERT#${HTTPD_SSL_CERT}#g' | \
		sed 's#HTTPD_SSL_KEY#${HTTPD_SSL_KEY}#g' | \
	tee ${HTTPD_DIR}/Apache24/conf/httpd.conf && \
	cat $(word 2, $^) | \
		sed 's#PHP_LOG_DIR#${PHP_LOG_DIR}#g' | \
		sed 's#PHP_EXTDIR#${PHP_EXTDIR}#g' | \
	tee ${PHP_DIR}/php.ini && \
	echo "WEBROOT => ${HTTPD_WEBROOT}" && \
	echo "SERVER  => ${HTTPD_IPADDR}:${HTTPD_IPPORT}" && \
	echo "WEBROOT_SSL => ${HTTPD_WEBROOT_SSL}" && \
	echo "SERVER_SSL  => ${HTTPD_IPADDR_SSL}:${HTTPD_IPPORT_SSL}" && \
	echo "HTTPD_LOG_DIR => ${HTTPD_LOG_DIR}" && \
	echo "PHP_LOG_DIR => ${PHP_LOG_DIR}" && \
	PHPRC=${PHP_DIR} ./${HTTPD_DIR}/Apache24/bin/httpd \
		-d ${HTTPD_DIR}/Apache24
